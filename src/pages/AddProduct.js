import { Row, Col, Container } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'
import { Form, Button } from 'react-bootstrap'


export default function AddProduct() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false)


	function addProduct(e) {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				Swal.fire({
					title: "Successfuly added a product",
					icon: "success",
					text: `${name} added`
				});

				navigate('/admin');

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})

		setName('')
		setDescription('')
		setPrice('')
	};


	useEffect(() => {
		if (name !== "" && description !== "" && price > 0) {
			setIsActive(true);
		} else {
			setIsActive(false)
		}
	}, [name, description, price]);


	return (
		user.isAdmin
		?
			<>
				<h1 className="my-5 text-center">Add Product</h1>

				<Container>
					<Row className="p-5 mt-3 mb-2">
						<Col md={10} className="border border-light rounded loginForm">
										<Form className=" p-3 m-3" onSubmit={(e) => addProduct(e)}>
										      <Form.Group className="mb-3" controlId="formBasicEmail">
										        <Form.Label>Product Name</Form.Label>
										        <Form.Control 
										        type="text" 
										        placeholder="Enter product name"				        
										        value = {name}
										        onChange = {e => setName(e.target.value)}
										        required
										         />	     			        
										      </Form.Group>   

										      <Form.Group className="mb-3" controlId="formBasicEmail">
										        <Form.Label>Description</Form.Label>
										        <Form.Control 
										        type="textarea" 
										        placeholder="Add description"	
										        value = {description}
										        onChange = {e => setDescription(e.target.value)}
										        required
										         />	     			        
										      </Form.Group>   

										      <Form.Group className="mb-3" controlId="formBasicEmail">
										        <Form.Label>Price</Form.Label>
										        <Form.Control 
										        type="number" 
										        placeholder="Enter price"	
										        value = {price}
										        onChange = {e => setPrice(e.target.value)}
										        required
										         />	     			        
										      </Form.Group>         

										            
										      { 
										      	isActive
										      	?
										      	<Button variant="primary" type="submit" id="submitBtn">Add</Button>
										      	:
										      	<Button variant="primary" type="submit" id="submitBtn" disabled>Add</Button>

						 				  	  }

						 				  	  <Button className="m-2" as={Link} to="/admin" variant="secondary" type="submit" id="submitBtn">
							        	    		Back
							        	    	</Button>
										      
										   
										    </Form>	
						</Col>
					</Row>
				</Container>
			</>
		:
			<Navigate to="/products/" />	


	)
};







