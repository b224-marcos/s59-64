import { useState, useEffect, useContext } from 'react'
import { Navigate, Link, useParams, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Form, Button, Container, Row, Col } from 'react-bootstrap'


export default function EditProduct() {

	const { courseId } = useParams();
	const { user } = useContext(UserContext)
	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0)
	const [isActive, setIsActive] = useState(false)

	function updateInfo(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})

		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				Swal.fire({
					title: "Update Successful!",
					icon: "success",
					text: "Congrats!"
				});

				navigate("/admin")

			} else {
				Swal.fire({
					title: "Something is wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})

		setName('');
		setDescription('');
		setPrice(0);

	}

	useEffect(() => {

		if(name !== '' && description !== '' && price > 0) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price]);


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}`)
		.then(res => res.json())
		.then(data => {
		console.log(data)
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	}, [courseId]);

	return (
		user.isAdmin
		?
			<>
				<h1 className="my-5 text-center">Update Product Information</h1>

				<Container>
					<Row className="p-5 mt-3 mb-2">
						<Col md={10} className="border border-light rounded loginForm">
							<Form onSubmit={(e) => updateInfo(e)}>

							<Form.Group className="mb-3" controlId="formBasicEmail">
							  <Form.Label>Product Name</Form.Label>
							  <Form.Control 
							  type="text" 
							  placeholder="Enter product name"				        
							  value = {name}
							  onChange = {e => setName(e.target.value)}
							  required
							   />	     			        
							</Form.Group>  

							<Form.Group className="mb-3" controlId="formBasicEmail">
							  <Form.Label>Description</Form.Label>
							  <Form.Control 
							  type="text" 
							  placeholder="Enter description"				        
							  value = {description}
							  onChange = {e => setDescription(e.target.value)}
							  required
							   />	     			        
							</Form.Group>  

							<Form.Group className="mb-3" controlId="formBasicEmail">
							  <Form.Label>Price</Form.Label>
							  <Form.Control 
							  type="text" 
							  placeholder="Enter Price"				        
							  value = {price}
							  onChange = {e => setPrice(e.target.value)}
							  required
							   />	     			        
							</Form.Group>  

							{
								       
							       
							      	isActive
							      	?
							      	<Button variant="primary" type="submit" id="submitBtn">Update</Button>
							      	:
							      	<Button variant="primary" type="submit" id="submitBtn" disabled>Update</Button>

								  	  

							}

							<Button className="m-2" as={Link} to="/admin" variant="secondary" type="submit" id="submitBtn">
										        	    		Back
										        	    	</Button>
							</Form>
						</Col>
					</Row>
				</Container>
			</>

		:
			<Navigate to="/products" />

	)

}