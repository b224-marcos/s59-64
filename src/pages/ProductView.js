import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function CourseView() {

	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling to a course
	const navigate = useNavigate(); // useHistory

	// The "useParams" hook allows us to retrieve the courseId passed via the URL params
	const { courseId } = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


	const purchase = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId : courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {

				Swal.fire({
					title: "Successful !",
					icon: "success",
					text: "Goodluck on your learning journey"
				})

				navigate("/products")

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(() => {

		console.log(courseId)

		fetch(`${process.env.REACT_APP_API_URL}/products/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [courseId])

	return (

		<Container>
			<Row className="mt-5">
				<Col lg={{span: 6, offset:3}} >
					<Card >											     	
					     	<Card.Body className="text-center">
					      	<Card.Title className="mb-5">{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        
					       
					        {
					        	(user.id !==null ) ?
					        	 <Button variant="primary" onClick={() => purchase(courseId)} >Purchase</Button>
					        	 :
					        	  <Button variant="outline-danger" as={Link} to="/login">Login to Purchase </Button>
					        }

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}