import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Learn a Language",
		content: "Learn a language right here with a number of language-learning lessons, Join us and have some fun.",
		destination: "/products",
		label: "Start Learning Now!"
	}


	return (

		<>
			<Banner data={data} />
			<Highlights/>
			
		</>

	)
}