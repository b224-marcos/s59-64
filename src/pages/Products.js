import { useEffect, useState, useContext } from 'react'
import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext'

export default function Courses() {

	const[courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {

				if(course.isActive) {
				return (
					<ProductCard key={course._id}courseProp={course} />

				)

				} else {
					return 
				}
				
			}))
		})
	},[])


	return (
		<>
		<h1 className="product-page-title">What we Offer</h1>
		{courses}

		</>

	)
};

