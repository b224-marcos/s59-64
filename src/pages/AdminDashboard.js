import { useEffect, useState, useContext } from 'react'
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom'

import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'

import ProductCard from '../components/ProductCard'
import UserContext from '../UserContext'
import AdminTable from '../components/AdminTable'

export default function AdminDashboard() {

	const[courses, setCourses] = useState([]);
	const navigate = useNavigate(); 
	
	function addCourse () {
		navigate('/add')
	}

// Makes a request to the database to get all the information about the courses AND refreshes the pages
	function showCourses() {

		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers: {
			Authorization: `Bearer ${localStorage.getItem('token')}`
			
		}})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data.map(course => {
				return (
					<AdminTable key={course._id}courseProp={course} refreshCoursesProp={showCourses} />

				)
			}))
		})

	}

	useEffect(() => {
		showCourses()
		
	},[])

	

	return (
		<>
		<h1 className="dashboard-title">Admin Dashboard</h1>
		 <Row className="text-center mt-2 mb-5">
		 	 <Col md={12}>
		 	 	<Button variant="outline-primary" onClick={addCourse}>Add Product</Button>
		 	 </Col>
		 </Row>
		 <Table striped bordered hover>
      		<thead>
        <tr className="bg-light">
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Availability</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
		{courses}
		      </tbody>
    </Table>

		</>

	)
};



