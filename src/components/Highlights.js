import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return (

		<>
		<Row>
			<Card className="text-center bg-secondary text-white my-5 py-2">
			  <Card.Body><h3>Saving the World One Language at a Time.</h3></Card.Body>
			</Card>
		</Row>

		<Row className="mt-4 mb-3">
			<Col xs={12} md={4} >
				
			<Card className="cardHighlight p-3">

		   		<Card.Body>
		   		 <Card.Img className="card-img-top img-fluid" src="https://i.pinimg.com/originals/67/cc/ff/67ccff444ae939fcb3d8a4ba6883a5a9.jpg"/>
		       	<Card.Title className="mt-5 text-center">Effective and Efficient</Card.Title>
		       	
		       	<Card.Text>
		        
		       	</Card.Text>
		       	</Card.Body>
			</Card>

			</Col>

			<Col xs={12} md={4} >
				
			<Card className="cardHighlight p-3">
		   		<Card.Body>
		   		 <Card.Img className="card-img-top img-fluid" src="https://tse3.mm.bing.net/th?id=OIP.9KusAH3N3I959MU-njmk3AHaHa&pid=Api&P=0" />
		       	<Card.Title className="mt-5 text-center">Personalised Learning</Card.Title>
		       	
		       	<Card.Text>
		         

		        </Card.Text>
		       	</Card.Body>
			</Card>

			</Col>

			<Col xs={12} md={4} >
				
			<Card className="cardHighlight p-3">
		   		<Card.Body>
		   		 <Card.Img className="card-img-top img-fluid" src="https://clipground.com/images/japanese-language-clipart-2.png" />
		       	<Card.Title className="mt-5 text-center">Stay Motivated</Card.Title>
		       	
		       	<Card.Text >
		        
		       	</Card.Text>
		       	</Card.Body>
			</Card>

			</Col>

		</Row>

		</>

		





	)
};