
import { Row, Col, Card, Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Navigate, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'


export default function AdminTable({courseProp, refreshCoursesProp}) {

	const {_id, name, description, price, isActive} = courseProp
	console.log(_id)

	function disable () {
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${_id}`, {
			method: 'PUT',
			headers: {
			'Content-type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
			
		},
			body: JSON.stringify({
			isActive: false
		})

	})
		.then(res => res.json())
		.then(data => console.log(data))
		.then(() => refreshCoursesProp())
	}

	

	function activate () {

		fetch(`${process.env.REACT_APP_API_URL}/products/active/${_id}`, {
			method: 'PUT',
			headers: {
			'Content-type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
			
		},
			body: JSON.stringify({
			isActive: true
		})

	})
		.then(res => res.json())
		.then(data => console.log(data)
			)
		.then(() => refreshCoursesProp())
	}


	return (
        <tr>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
         {(isActive) ? <td>Available </td> : <td> Not Available </td> } 
          <td> <Button variant="primary" as={Link} to={`/editProduct/${_id}`}>Update</Button>{(isActive)? <Button variant="danger" onClick={disable}>Disable</Button> : <Button variant="success" onClick={activate}>Activate</Button>}</td>
        </tr> 
)




}