import { useState, useEffect } from 'react'
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

	

	const { name, description, price, _id } = courseProp;

	return (

	<Row className="justify-content-center mb-4">
		<Col  xs={12} md={3} className="w-50 text-center">
			<Card className="card p-3" >
		      <Card.Body>
		      		
			        <Card.Title>{name}</Card.Title>
			        <Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>{description}</Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>{price}</Card.Text>
			        <Button className="bg-secondary" as={Link} to={`/products/${_id}`}>Details</Button>
			   </Card.Body>
			</Card>
		</Col>
	</Row>
		

	)
}
