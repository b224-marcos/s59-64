import {Row, Col, Button, Container, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner({data}) {

	//console.log(data);
	const {title, content, destination, label} = data;

	return (

		 <Container>
		      <Row className= "px-1 my-5">
		        <Col sm={7}>
		        	<Image src="https://tse4.mm.bing.net/th?id=OIP.6V2JGsyjbASOgvQOygEd5wHaD2&pid=Api&P=0" fluid rounded />
		        </Col>
		        <Col sm={5}>
		        	<h1 className="pb-2">{title}</h1>
					<p className= "pb-2">{content}</p>
					<Button className="text-center" variant="outline-primary" as={Link} to={destination} >{label}</Button>
		        </Col>
		      </Row>
		     
		    </Container>
		
	)

}




 